# Given the root of a binary tree, invert the tree, and return its root.


class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        if root is None:
            return
        temp = root.left
        temp2 = root.right
        root.left = self.invertTree(temp2)
        root.right = self.invertTree(temp)
        return root
