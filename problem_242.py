# Given two strings s and t, return true if t is an anagram of s, and false otherwise.

# An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.


class Solution(object):
    def isAnagram(self, s, t):
        if len(s) != len(t):
            return False
        hashmap = {}
        for letter in s:
            if letter in hashmap:
                hashmap[letter] += 1
            else:
                hashmap[letter] = 1
        hashmap2 = {}
        for letter in t:
            if letter in hashmap2:
                hashmap2[letter] += 1
            else:
                hashmap2[letter] = 1
        if hashmap == hashmap2:
            return True
        return False
