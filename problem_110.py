# Given the root of a binary tree, return the length of the diameter of the tree.

# The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

# The length of a path between two nodes is represented by the number of edges between them.


class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        def depth(node):
            if node == None:
                return 0
            return max(depth(node.left), depth(node.right)) + 1

        if root == None:
            return True
        l = depth(root.left)
        r = depth(root.right)
        return (
            (abs(l - r) < 2)
            and self.isBalanced(root.left)
            and self.isBalanced(root.right)
        )
